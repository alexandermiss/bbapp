var mongoose = require('mongoose');

var Employee = new mongoose.Schema({
	name: String,
	lastname: String
});

module.exports = mongoose.model('employee', Employee);