var express = require('express');
var router = express.Router();

var Employee = require('../models/Employee.js');

router.get('/employees', function(req, res) {

	Employee.find({}).exec(function(err, emps){
		if (err) return errorHandler(err);
		res.json(emps);
	});

});

router.post('/employee', function(req, res) {

	var employee = new Employee({ name: req.param('name'), lastname: req.param('lastname')});

	employee.save(function (err) {
	  if (err) res.json(err);
	   res.json(employee);
	});

});

router.put('/employee/:id', function(req, res) {

	Employee.update({ _id: req.param('id')}, {name: req.param('name'), lastname: req.param('lastname') }, function (err) {
		  if (err) return handleError(err);
		  // removed!
		  res.json({ status: 'success' });
		});

res.json({ emps: req.param('id')});
});

router.delete('/employee/:id', function(req, res) {

		Employee.remove({ _id: req.param('id') }, function (err) {
		  if (err) return handleError(err);
		  // removed!
		  res.json({ status: 'success' });
		});

});

router.post('/formdata', function(req, res) {

		// res.json({response: req.param('datas')});
		var datas = req.param('datas');

		for ( var i in datas.empleado )
			console.log(i);



});

module.exports = router;
