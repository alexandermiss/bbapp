var app = app || {};

$(function(){
	'use strict';

	new app.AppView;

	app.employees.fetch({ reset: true });
});

