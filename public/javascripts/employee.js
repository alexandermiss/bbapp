var app = app || {};

(function(){
	'use strict';

	app.Employee = Backbone.Model.extend({

		idAttribute: '_id',

		urlRoot: '/api/v1/employee',

		validate: function (attrs, options){
			if( _.isEmpty( attrs.name ) ){
				return 'Especifica un nombre de empleado';
			}
		}

	});
	
})();

