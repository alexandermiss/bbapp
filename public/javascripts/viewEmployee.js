var app = app || {};

(function($){
	'use strict';

	app.EmployeeView = Backbone.View.extend({

		tagName: 'tr',

		events:{
			'click .btn-danger' : 'removeItem'
		},

		template: _.template( $('#my-template').html() ),

		initialize: function (){
			this.listenTo(this.model, 'change', this.render);
		},

		render: function (){
			this.$el.html( this.template( this.model.toJSON() ) );
			return this;
		},

		removeItem: function (ev){
			ev.preventDefault();
			this.$el.fadeOut('slow').slideUp('slow');
			this.model.destroy();
		},

	});

})(jQuery);

