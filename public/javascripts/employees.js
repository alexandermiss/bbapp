var app = app || {};

(function(){
	'use strict';

	_.templateSettings = {
	  interpolate: /\{\{(.+?)\}\}/g
	};

	var Employees = Backbone.Collection.extend({
		model: app.Employee,
		url: '/api/v1/employees'
	});

	app.employees = new Employees();
})();

