var app = app || {};

(function($){
	'use strict';

	app.AppView = Backbone.View.extend({
		el: '#app',

		events: {
			'click #newEmployee'		: 'newEmployee'
		},

		initialize: function (){
			this.$nameEmployee = $('#name');
			this.$lastnameEmployee = $('#lastname');
			this.$table = $('tbody');
			this.listenTo(app.employees, 'add', this.addOne);
			this.listenTo(app.employees, 'reset', this.addAll);

		},

		addOne: function (model){

			if ( model.isValid() ){
				var myView = new app.EmployeeView({ model: model });
				this.$table.append( myView.render().el );			
			}else{
				alert(model.validationError);
			}

		},
		
		newEmployee: function (e){
			e.preventDefault();
			app.employees.create( this.getEmployee() );
			this.$lastnameEmployee.val('');
			this.$nameEmployee.val('');
			this.$nameEmployee.focus();
		},

		getEmployee: function (){
			var lastname = this.$lastnameEmployee.val();
			return {
				name: this.$nameEmployee.val(),
				lastname: _.isEmpty(lastname) ? 'Sin especificar' : lastname
			}
		},

		removeEmployee: function (){
			console.log('log desde collection');
		},

		addAll: function (){
			app.employees.each(this.addOne, this);
		}

	});

})(jQuery);

